import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RawArticle } from "../models/raw-article";
import { ArticleService } from '../services/article.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-article-management',
  templateUrl: './article-management.component.html',
  styleUrls: ['./article-management.component.css']
})
export class ArticleManagementComponent implements OnInit {

  articleForm : FormGroup;
  _creationMode : boolean = true;

  constructor(private fb: FormBuilder, private articleService: ArticleService, private route: ActivatedRoute, private router: Router) {
   this.articleForm = this.fb.group({
     title: ['Fake Title', Validators.required ],
     content : ['', Validators.required ],
     authors : ['', Validators.required ],
   });
  }

  createArticle(){
    const formModel = this.articleForm.value;
    const rawArticle : RawArticle = {
      title : formModel.title,
      content : formModel.content,
      authors : formModel.authors
    }
    this.articleService.add(rawArticle).subscribe();
  }

  updateArticle(){
    const formModel = this.articleForm.value;
    const rawArticle : RawArticle = {
      title : formModel.title,
      content : formModel.content,
      authors : formModel.authors
    }
    this.route.params.subscribe( params => {
        this.articleService.update(params['id'], rawArticle).subscribe();
    });

  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      if(params && params['id']){
        this._creationMode = false;
      }
    });
  }

}

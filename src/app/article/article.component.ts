import {Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import {Article} from '../models/article';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../services/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input()
  article: Article;

  @Output()
  deletedArticle : EventEmitter<Article> = new EventEmitter();

  constructor(private route: ActivatedRoute, private articleService: ArticleService) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      if(params && params['id']){
        this.articleService.get(params['id']).subscribe((fetchedArticle)=> { this.article = fetchedArticle; });
      }
    });
  }

  delete(){
    this.route.params.subscribe( params => {
        if(params && params['id']){
          this.articleService.delete(this.article.id).subscribe();
        }else{
          this.deletedArticle.emit(this.article);
        }
    });
  }
}

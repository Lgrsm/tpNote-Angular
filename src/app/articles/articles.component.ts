import {Component, OnInit} from '@angular/core';
import {Article} from "../models/article";
import {ArticleService } from "../services/article.service";
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  searchForm : FormGroup;
  _searchFilter : string = "";
  private _articles : Observable<Article[]>;

  constructor(private articleService: ArticleService, private fb : FormBuilder) {
    this.searchForm = this.fb.group({
       search: ['', Validators.required ]
     });
  }

  articles(): Observable<Article[]> {
    return this._articles;
  }

  ngOnInit() {
    this._articles = this.articleService.getAll();
  }

  delete(article: Article){
    this.articleService.delete(article.id).subscribe(()=>{
      this._articles = this.articleService.getAll();
    });
  }

  search() {
    this._searchFilter = this.searchForm.value.search.toLowerCase();
    console.log(this._searchFilter);
  }

}
